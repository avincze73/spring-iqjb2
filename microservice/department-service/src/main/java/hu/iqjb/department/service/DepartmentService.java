package hu.iqjb.department.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Service
public class DepartmentService {

    @Autowired
    private DepartmentRepository departmentRepository;

    @Transactional(propagation = Propagation.REQUIRED)
    public void saveDepartment(){
        Department department = new Department();
        department.setName("emp1");
        departmentRepository.save(department);
    }
}
