# Inversion of control

## di-demo

### Component lookup in IoC container.
- Main1

### Constructor-based dependency injection
- Main2

### Injecting simple types
- Main3

### Setter injection of simple types
- Main4

### Setter injection of reference types
- Main5

### Injection collection types
- Main6

### Injecting inner beans
- The bean is private it is not distributed among others. 
- Defined in the enclosing bean context 
- It does not have id because it is not shared. 
- Inner bean is not the same as inner class. Printer class in not an inner class but the printer bean is an inner bean. 
- Inner bean can not be reused.
- Main7

### Injecting empty or null string values
- Main8

### Inheritance in bean definition (abstract included)
- Main9

### Autowiring
- Main10

### Spring bean life cycle
- UserServiceImplWithInit
- UserServiceImplWithInit2
- Main11


