package hu.iqjb.ioc;

import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

@Component
public class LoginEventListener implements ApplicationListener<LoginEvent> {

    public void onApplicationEvent(LoginEvent event) {
        System.out.println(event);

    }
}
