package hu.iqjb.ex1;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class EmployeeService {
	private EmployeeRepository repository;

	public void add(Employee employee) {
		repository.add(employee);
	}

	public List<Employee> findAll() {
		return repository.getAll();
	}
}
