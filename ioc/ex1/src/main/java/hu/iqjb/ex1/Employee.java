package hu.iqjb.ex1;


import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
@ToString
public class Employee {
	private int id;
	private String firstName;
	private String lastName;
	private String loginName;
	private String password;
	private String title;
	private double salary;

}
