package hu.iqjb.ex1;

import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Getter
@Setter
public class InMemoryEmployeeRepository implements EmployeeRepository {

	private Set<Employee> database = new HashSet<>();
	
	@Override
	public void add(Employee employee) {
		database.add(employee);
	}

	@Override
	public List<Employee> getAll() {
		return new ArrayList(database);
	}


}
