package hu.iqjb.ex1;

import java.util.List;

public interface EmployeeRepository {
	void add(Employee employee);

	List<Employee> getAll();
}
