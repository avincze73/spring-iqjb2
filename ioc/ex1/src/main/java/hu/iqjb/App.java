package hu.iqjb;

import hu.iqjb.ex1.Employee;
import hu.iqjb.ex1.EmployeeService;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        ApplicationContext context =
                new ClassPathXmlApplicationContext("beans.xml");
        EmployeeService employeeService =
                context.getBean("employeeService",
                        EmployeeService.class);
        Employee employee = new Employee();
        employee.setId(6);
        employee.setFirstName("firstName6");
        employeeService.add(employee);
        employeeService.findAll().forEach(System.out::println);
    }
}
