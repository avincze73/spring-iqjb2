package hu.iqjb.ioc;

import org.springframework.context.ApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Main8 {

	public static void main(String[] args) {
		ApplicationContext context = new ClassPathXmlApplicationContext("beans.xml");


		EmployeeWithNull employee7 = (EmployeeWithNull) context.getBean("employee4");
		System.out.println(employee7);

		EmployeeWithNull employee8 = (EmployeeWithNull) context.getBean("employee5");
		System.out.println(employee8);

		((ConfigurableApplicationContext)context).close();
		
		
	}
}
