package hu.iqjb.ioc;

import org.springframework.context.ApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Main5 {

	public static void main(String[] args) {
		ApplicationContext context = new ClassPathXmlApplicationContext("beans.xml");

		UserService usrService = (UserService) context.getBean("userService2");
		System.out.println("Unique User Id: " + usrService.getId());
		

		((ConfigurableApplicationContext)context).close();
		
		
	}
}
