package hu.iqjb.ioc;

import org.springframework.context.ApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Main10 {

    public static void main(String[] args) {
        ApplicationContext context = new ClassPathXmlApplicationContext("beans.xml");

        UserServiceImplWithAutowire1 u = (UserServiceImplWithAutowire1) context.getBean("userServiceWithAutowire1");
        System.out.println(u.getUserDao());

        UserServiceImplWithAutowire2 u1 = (UserServiceImplWithAutowire2) context.getBean("userServiceWithAutowire2");
        System.out.println(u1.getUserDao());


        ((ConfigurableApplicationContext) context).close();


    }
}
