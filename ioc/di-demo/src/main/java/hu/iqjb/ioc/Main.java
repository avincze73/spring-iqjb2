package hu.iqjb.ioc;

import org.springframework.context.ApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Main {

	public static void main(String[] args) {
		ApplicationContext context = new ClassPathXmlApplicationContext("beans.xml");

		UserService usrService = (UserServiceImpl) context.getBean("userServiceBean");
		System.out.println("Unique User Id: " + usrService.getId());
		
		UserService usrService2 = (UserServiceImpl2) context.getBean("userService");
		System.out.println("Unique User Id: " + usrService2.getId());
		
		Employee employee = (Employee) context.getBean("employee");
		System.out.println(employee);
		
		
		Employee2 employee2 = (Employee2) context.getBean("employee2");
		System.out.println(employee2);
		
		
		EmployeeWithCollections employee3 = (EmployeeWithCollections) context.getBean("employee3");
		System.out.println(employee3);
		
		Atm atm = (Atm) context.getBean("atmBean");
		atm.printBalance("1123");
		
		EmployeeWithInheritance employee7 = (EmployeeWithInheritance) context.getBean("employee7");
		System.out.println(employee7);
		
		UserServiceImplWithAutowire2 u1 = (UserServiceImplWithAutowire2) context.getBean("userServiceWithAutowire2");
		System.out.println(u1.getUserDao());
		
		UserServiceImplWithAutowire1 u = (UserServiceImplWithAutowire1) context.getBean("userServiceWithAutowire1");
		System.out.println(u.getUserDao());
		
		((ConfigurableApplicationContext)context).close();
		
		
	}
}
