package hu.iqjb.ioc;

public class Employee2 {

	private String employeeName;
	private int employeeAge;
	private boolean married;

	public String getEmployeeName() {
		return employeeName;
	}

	public void setEmployeeName(String employeeName) {
		this.employeeName = employeeName;
	}

	public int getEmployeeAge() {
		return employeeAge;
	}

	public void setEmployeeAge(int employeeAge) {
		this.employeeAge = employeeAge;
	}

	public boolean isMarried() {
		return married;
	}

	public void setMarried(boolean married) {
		this.married = married;
	}

	@Override
	public String toString() {

		return "Employee Name: " + this.employeeName + " , Age:" + this.employeeAge + ", IsMarried: " + married;
	}
}
