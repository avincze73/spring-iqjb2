package hu.iqjb.ioc;

import org.springframework.context.ApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Main7 {

	public static void main(String[] args) {
		ApplicationContext context = new ClassPathXmlApplicationContext("beans.xml");

		Atm atm = (Atm) context.getBean("atmBean");
		atm.printBalance("1123");

		((ConfigurableApplicationContext)context).close();
		
		
	}
}
