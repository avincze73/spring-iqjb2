package hu.iqjb.ioc;

import org.springframework.context.ApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Main2 {

	public static void main(String[] args) {
		ApplicationContext context = new ClassPathXmlApplicationContext("beans.xml");

		UserService userService = (UserServiceImpl2) context.getBean("userService");
		System.out.println("Unique User Id: " + userService.getId());

		((ConfigurableApplicationContext)context).close();
	}
}
