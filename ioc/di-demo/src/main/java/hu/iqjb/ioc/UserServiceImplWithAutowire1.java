package hu.iqjb.ioc;

public class UserServiceImplWithAutowire1 implements UserService {

    private UserDao userDao;

    public UserServiceImplWithAutowire1() {
        super();
    }

    public UserDao getUserDao() {
        return userDao;
    }

    public void setUserDao(UserDao userDao) {
        this.userDao = userDao;
    }

    public Long getId() {
        return System.currentTimeMillis();
    }

}
