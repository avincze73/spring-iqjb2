package hu.iqjb.ioc;

public class UserServiceImplWithAutowire2 implements UserService {
	private UserDao userDao;

	public UserServiceImplWithAutowire2(UserDao userDao) {
		super();
		this.userDao = userDao;
	}

	public UserDao getUserDao() {
		return userDao;
	}

	public Long getId() {
		return System.currentTimeMillis();
	}

}
