package hu.iqjb.ioc;

public class EmployeeWithNull {

	private String employeeName;

	

	public String getEmployeeName() {
		return employeeName;
	}



	public void setEmployeeName(String employeeName) {
		this.employeeName = employeeName;
	}



	@Override
	public String toString() {

		return "Employee Name: " + this.employeeName;
	}
}
