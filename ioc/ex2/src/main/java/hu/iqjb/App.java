package hu.iqjb;

import hu.iqjb.ex2.Employee;
import hu.iqjb.ex2.EmployeeService;
import hu.iqjb.ex2.MyConfig;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class App 
{
    public static void main( String[] args )
    {
        ApplicationContext context2 = new AnnotationConfigApplicationContext(MyConfig.class);
        EmployeeService employeeService2 = context2.getBean("employeeService",EmployeeService.class);
        Employee employee = new Employee();
        employee.setId(6);
        employee.setFirstName("firstName6");
        employeeService2.add(employee);
        employeeService2.getAll().forEach(System.out::println);
    }
}
