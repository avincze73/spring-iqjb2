package hu.iqjb.ex2;

import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import java.util.Set;

@Service
@Scope(scopeName="prototype")
@Setter
public class EmployeeService {
	
	@Autowired
	private EmployeeRepository employeeRepository;

	public void add(Employee employee) {
		employeeRepository.add(employee);
	}

	public Set<Employee> getAll() {
		return employeeRepository.getAll();
	}

}
