package hu.iqjb.ex2;

import java.util.Set;

public interface EmployeeRepository {
	void add(Employee employee);
	Set<Employee> getAll();

}
