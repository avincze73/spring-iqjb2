package hu.iqjb.ex2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;

import java.util.HashSet;
import java.util.Set;

@Configuration
@ComponentScan("hu.iqjb.ex2")
public class MyConfig {


    @Bean
    public Employee employee1() {
        Employee emp = new Employee();
        emp.setId(1);
        emp.setFirstName("firstName1");
        emp.setLastName("lastName1");
        emp.setLoginName("loginName1");
        emp.setPassword("password1");
        emp.setSalary(10);
        return emp;
    }

    @Bean
    public Employee employee2() {
        Employee emp = new Employee();
        emp.setId(2);
        emp.setFirstName("firstName2");
        emp.setLastName("lastName2");
        emp.setLoginName("loginName2");
        emp.setPassword("password2");
        emp.setSalary(20);
        return emp;
    }

    @Bean
    public Employee employee3() {
        Employee emp = new Employee();
        emp.setId(3);
        emp.setFirstName("firstName3");
        emp.setLastName("lastName3");
        emp.setLoginName("loginName3");
        emp.setPassword("password3");
        emp.setSalary(30);
        return emp;
    }

//    @Bean
//    public EmployeeService employeeService() {
//        EmployeeService employeeService = new EmployeeService();
//        System.out.println(employeeService.hashCode());
//        //System.out.println(environment.getActiveProfiles()[0]);
//        return employeeService;
//    }

    @Bean
    public EmployeeRepository inMemoryRepository() {
        InMemoryEmployeeRepository employeeRepository = new InMemoryEmployeeRepository();
        employeeRepository.setDatabase(mySet());
        return employeeRepository;
    }

    @Bean
    public Set<Employee> mySet() {
        Set<Employee> mySet = new HashSet<>();
        mySet.add(employee1());
        mySet.add(employee2());
        mySet.add(employee3());
        return mySet;
    }

}
