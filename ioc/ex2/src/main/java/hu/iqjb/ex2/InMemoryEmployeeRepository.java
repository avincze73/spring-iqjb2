package hu.iqjb.ex2;

import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.Set;


@Setter
public class InMemoryEmployeeRepository implements EmployeeRepository {

	@Autowired
	private Set<Employee> database;

	public void add(Employee employee) {
		// TODO Auto-generated method stub
		database.add(employee);
	}

	public Set<Employee> getAll() {
		// TODO Auto-generated method stub
		return database;
	}

}
