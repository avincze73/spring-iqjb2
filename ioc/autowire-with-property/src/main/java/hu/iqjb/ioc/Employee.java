package hu.iqjb.ioc;

import org.springframework.stereotype.Component;

@Component
public class Employee {

    @Override
    public String toString() {
        return "From employee class";
    }

}
