package hu.iqjb.mongo;

import com.mongodb.client.MongoClients;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.SimpleMongoClientDbFactory;

import java.util.List;
import java.util.logging.Logger;

import static org.springframework.data.mongodb.core.query.Criteria.where;
import static org.springframework.data.mongodb.core.query.Query.query;
import static org.springframework.data.mongodb.core.query.Update.update;

/**
 * Hello world!
 */
public class App3 {

    private static final Logger log = Logger.getLogger(App3.class.getName());


    public static void main(String[] args) {
        AnnotationConfigApplicationContext applicationContext = new AnnotationConfigApplicationContext(MongoRepositoryConfig.class);

        PersonService personService = applicationContext.getBean("personService", PersonService.class);
        personService.save();
        personService.getAll();


    }
}
