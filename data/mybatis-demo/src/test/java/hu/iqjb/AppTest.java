package hu.iqjb;

import hu.iqjb.mybatis.Article;
import hu.iqjb.mybatis.ArticleMapper;
import hu.iqjb.mybatis.MyConfiguration;
//import org.junit.Test;
//import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.assertj.core.api.Assertions.assertThat;
//import static org.junit.Assert.assertTrue;


/**
 * Unit test for simple App.
 */
//@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = MyConfiguration.class)
public class AppTest 
{


    @Autowired
    private ArticleMapper articleMapper;


    //@Test
    public void shouldAnswerWithTrue()
    {
       //assertTrue( true );
    }


    //@Test
    public void whenRecordsInDatabase_shouldReturnArticleWithGivenId() {
        Article article = articleMapper.getArticle(1L);

        assertThat(article).isNotNull();
        assertThat(article.getId()).isEqualTo(1L);
        assertThat(article.getAuthor()).isEqualTo("IQJB");
        assertThat(article.getTitle()).isEqualTo("Working with MyBatis in Spring");
    }
}
