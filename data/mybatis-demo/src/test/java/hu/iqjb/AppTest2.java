package hu.iqjb;

import hu.iqjb.mybatis.Article;
import hu.iqjb.mybatis.ArticleMapper;
import hu.iqjb.mybatis.MyConfiguration;
import hu.iqjb.mybatis.MyService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.assertj.core.api.Assertions.assertThat;


/**
 * Unit test for simple App.
 */
@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = MyTestConfiguration.class)
public class AppTest2
{

    @Autowired
    private MyService myService;

    @Autowired
    private ArticleMapper articleMapper;


    @Test
    //Smoke test
    public void shouldAnswerWithTrue()
    {
        assertThat(myService).isNotNull();
        assertThat(articleMapper).isNotNull();
        assertThat( true );
    }


    @Test
    public void whenRecordsInDatabase_shouldReturnArticleWithGivenId() {
        Article article = articleMapper.getArticle(1L);

        assertThat(article).isNotNull();
        assertThat(article.getId()).isEqualTo(1L);
        assertThat(article.getAuthor()).isEqualTo("IQJB");
        assertThat(article.getTitle()).isEqualTo("Working with MyBatis in Spring");
    }
}
