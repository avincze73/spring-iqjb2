package hu.iqjb.spring.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.Repository;

import java.util.List;

public interface CustomerRepositorySpring extends Repository<Customer, Long> {
    void save(Customer customer);

    List<Customer> findAll();

    Customer findByFirstname(String firstName);


    @Query("select c from Customer c where c.lastname = ?1")
    Customer findByLastname(String lastName);

    //findBy, readBy, and getBy are allowed
    //Between, LessThan, GreaterThan, and Like expressions are allowed
    List<Customer> findByFirstnameAndLastname(String firstName, String lastName);

    //x.address.zipCode
    //List<Customer> findByAddressZipCode(ZipCode zipCode);

    //Resolving ambiguity if exists
    //List<Customer> findByAddress_ZipCode(ZipCode zipCode);


    Page<Customer> findByEmailAddress(String emailAddress, Pageable pageable);

    List<Customer> findByAddress(String address, Sort sort);

    List<Customer> findByAddress(String address, Pageable pageable);
}
