package hu.iqjb.testing.demo;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
@RunWith(Suite.class)
@Suite.SuiteClasses(TestingEmployeeRepositoryIntegrationTests.class)
class TestingDemoApplicationTests {

	@Test
	void contextLoads() {
	}

}
