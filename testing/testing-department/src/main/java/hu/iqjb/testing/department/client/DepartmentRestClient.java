package hu.iqjb.testing.department.client;

import hu.iqjb.testing.department.entity.Department;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class DepartmentRestClient {


    @Autowired
    private RestTemplate restTemplate;

    public Department getDepartmentById(Integer id) {
        ResponseEntity<Department> response
                = restTemplate.getForEntity("/api/departments/" + id, Department.class);
        return response.getBody();
    }

}
