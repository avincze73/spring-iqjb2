package hu.iqjb.testing.department.json;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import org.springframework.boot.jackson.JsonComponent;

import java.io.IOException;

@JsonComponent
public class CarDetailsJsonSerializer extends JsonSerializer<CarDetails> {
    @Override
    public void serialize(CarDetails carDetails, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException {
        jsonGenerator.writeStartObject();
        jsonGenerator.writeStringField(
                "type", carDetails.getManufacturer() + "|" + carDetails.getType() + "|" + carDetails.getColor());
        jsonGenerator.writeEndObject();
    }
}
