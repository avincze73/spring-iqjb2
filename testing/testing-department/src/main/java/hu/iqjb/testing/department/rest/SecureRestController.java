package hu.iqjb.testing.department.rest;


import hu.iqjb.testing.department.entity.Department;
import hu.iqjb.testing.department.service.DepartmentService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api2")
@Slf4j
public class SecureRestController {

    @GetMapping
    public String getHello() {
        return "Hello from secure controller";
    }

}
