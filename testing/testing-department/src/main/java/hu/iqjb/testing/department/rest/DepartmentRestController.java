package hu.iqjb.testing.department.rest;


import hu.iqjb.testing.department.entity.Department;
import hu.iqjb.testing.department.service.DepartmentService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/departments")
@Slf4j
public class DepartmentRestController {

    @Autowired
    private DepartmentService departmentService;

    @GetMapping
    public List<Department> getAllDepartments() {
        return departmentService.getAll();
    }


    @GetMapping("/{id}")
    public ResponseEntity<Department> getDepartmentsById(@PathVariable Integer id) {
        return departmentService.getById(id)
                .map( department -> ResponseEntity.ok(department))
                .orElseGet(() -> ResponseEntity.notFound().build());
    }

    @PostMapping
    //@PreAuthorize("hasRole('USER')")
    @ResponseStatus(HttpStatus.CREATED)
    public Department createDepartment(@RequestBody @Validated Department department) {
        return departmentService.save(department);
    }

    @PutMapping("/{id}")
    public ResponseEntity<Department> updateDepartment(@PathVariable Integer id, @RequestBody Department department) {
        return departmentService.getById(id)
                .map(departmentObj -> {
                    departmentObj.setId(id);
                    return ResponseEntity.ok(departmentService.update(departmentObj));
                })
                .orElseGet(() -> ResponseEntity.notFound().build());
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Department> deleteDepartment(@PathVariable Integer id) {
        return departmentService.getById(id)
                .map(department -> {
                    departmentService.deleteById(id);
                    return ResponseEntity.ok(department);
                })
                .orElseGet(() -> ResponseEntity.notFound().build());
    }
}
