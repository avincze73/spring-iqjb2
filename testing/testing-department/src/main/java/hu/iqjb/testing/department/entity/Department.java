package hu.iqjb.testing.department.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;

@Entity
@Table(name = "Department")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Department {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    Integer id;

    @NotEmpty(message = "Name should not be empty")
    @Column(name = "name")
    String name;

    @NotEmpty(message = "Leader should not be empty")
    @Column(name = "leader")
    String leader;


    public Department(String name) {
        this.name = name;
    }

    public Department(@NotEmpty(message = "Name should not be empty") String name,
                      @NotEmpty(message = "Leader should not be empty") String leader) {
        this.name = name;
        this.leader = leader;
    }
}
