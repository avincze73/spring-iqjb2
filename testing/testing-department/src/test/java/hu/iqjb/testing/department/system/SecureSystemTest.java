package hu.iqjb.testing.department.system;

import hu.iqjb.testing.department.entity.Department;
import hu.iqjb.testing.department.service.DepartmentService;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.ResponseEntity;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@Slf4j
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class SecureSystemTest {

    @Autowired
    private MockMvc mockMvc;


    @WithMockUser(value = "admin")
    @Disabled
    @Test
    public void givenAuthRequestOnPrivateService_shouldSucceedWith200() throws Exception {
        this.mockMvc.perform(get("/api2"))
                .andExpect(status().isOk());
    }

}
