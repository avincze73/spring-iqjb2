package hu.iqjb.testing.department.client;

import com.fasterxml.jackson.databind.ObjectMapper;
import hu.iqjb.testing.department.entity.Department;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.client.RestClientTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.client.MockRestServiceServer;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.requestTo;
import static org.springframework.test.web.client.response.MockRestResponseCreators.withSuccess;

@RestClientTest(DepartmentRestClient.class)
public class DepartmentRestClientTest {

    @Autowired
    private DepartmentRestClient client;

    @Autowired
    private MockRestServiceServer server;

    @Autowired
    private ObjectMapper objectMapper;

    @BeforeEach
    public void setUp() throws Exception {
        String detailsString =
                objectMapper.writeValueAsString(new Department("name1", "leader1"));

        this.server.expect(requestTo("/api/departments/1"))
                .andRespond(withSuccess(detailsString, MediaType.APPLICATION_JSON));
    }

    @Test
    public void whenCallingGetDepartmentById_thenClientMakesCorrectCall()
            throws Exception {

        Department department = this.client.getDepartmentById(1);

        assertThat(department.getName()).isEqualTo("name1");
        assertThat(department.getLeader()).isEqualTo("leader1");
    }
}
