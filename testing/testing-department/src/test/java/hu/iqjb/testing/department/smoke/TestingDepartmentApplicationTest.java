package hu.iqjb.testing.department.smoke;

import hu.iqjb.testing.department.repository.DepartmentRepository;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.logging.Logger;

@SpringBootTest
class TestingDepartmentApplicationTest {

    private static final Logger logger = Logger.getLogger(TestingDepartmentApplicationTest.class.getName());

    @Autowired
    private DepartmentRepository departmentRepository;

    @Test
        //it is used for smoke test
    void contextLoads() {
        logger.info("KKKKKKKKKK");

        Assertions.assertThat(departmentRepository).isNotNull();
    }

}
