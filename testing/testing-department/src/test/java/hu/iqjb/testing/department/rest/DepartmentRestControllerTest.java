package hu.iqjb.testing.department.rest;

import com.fasterxml.jackson.databind.ObjectMapper;
import hu.iqjb.testing.department.entity.Department;
import hu.iqjb.testing.department.repository.DepartmentRepositoryTest;
import hu.iqjb.testing.department.service.DepartmentService;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.data.mongo.DataMongoTest;
import org.springframework.boot.test.autoconfigure.web.client.RestClientTest;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.hasSize;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.doNothing;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(controllers = DepartmentRestController.class)
//@RestClientTest
//@RunWith(SpringRunner.class)
@ActiveProfiles("test")
@Slf4j
class DepartmentRestControllerTest {


    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private DepartmentService departmentService;

    @Autowired
    private ObjectMapper objectMapper;

    private List<Department> departmentList;


    @BeforeEach
    void setUp() {
        this.departmentList = new ArrayList<>();
        this.departmentList.add(new Department("dep1"));
        this.departmentList.add(new Department("dep2"));
        this.departmentList.add(new Department("dep3"));
    }

    @Test
    void shouldFetchAllDepartments() throws Exception {

        given(departmentService.getAll()).willReturn(departmentList);

        this.mockMvc.perform(get("/api/departments"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.size()", is(departmentList.size())));

    }


    @Test
    void shouldFetchOneDepartmentById() throws Exception {
        Integer id = 1;
        Department department = new Department("dep1");
        department.setId(id);
        department.setLeader("leader1");

        given(departmentService.getById(id)).willReturn(Optional.of(department));

        this.mockMvc.perform(get("/api/departments/{id}", id))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.name", is(department.getName())));
    }


    @Test
    void shouldReturn404WhenFindDepartmentById() throws Exception {
        Integer id = 2;
        given(departmentService.getById(id)).willReturn(Optional.empty());

        this.mockMvc.perform(get("/api/departments/{id}", id))
                .andExpect(status().isNotFound());
    }

    @Test
    void shouldCreateNewDepartment() throws Exception {
        given(departmentService.save(any(Department.class))).willAnswer((invocation) -> invocation.getArgument(0));

        Department department = new Department("dep4", "aa");

        this.mockMvc.perform(post("/api/departments")
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(objectMapper.writeValueAsString(department)))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.name", is(department.getName())));
    }


    @Test
    void shouldReturn400WhenCreateNewDepartmentWithEmptyLeader() throws Exception {
        Department department = new Department("cc", "");

        this.mockMvc.perform(post("/api/departments")
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(objectMapper.writeValueAsString(department)))
                .andExpect(status().isBadRequest())
                .andReturn()
        ;
    }


    @Test
    void shouldUpdateDepartment() throws Exception {
        Integer id = 1;
        Department department = new Department("cc", "fff");
        department.setId(1);
        given(departmentService.getById(id)).willReturn(Optional.of(department));
        given(departmentService.update(any(Department.class))).willAnswer((invocation) -> invocation.getArgument(0));

        this.mockMvc.perform(put("/api/departments/{id}", department.getId())
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(objectMapper.writeValueAsString(department)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.name", is(department.getName())))
                .andExpect(jsonPath("$.leader", is(department.getLeader())))
                .andExpect(jsonPath("$.id", is(department.getId())));

    }



    @Test
    void shouldReturn404WhenUpdatingNonExistingDepartment() throws Exception {
        Integer id = 4;
        given(departmentService.getById(id)).willReturn(Optional.empty());
        Department department = new Department("cc", "fff");
        department.setId(id);

        this.mockMvc.perform(put("/api/departments/{id}", id)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(objectMapper.writeValueAsString(department)))
                .andExpect(status().isNotFound());

    }



    @Test
    void shouldDeleteUser() throws Exception {
        Integer id = 1;
        Department department = new Department("cc", "fff");
        department.setId(id);
        given(departmentService.getById(id)).willReturn(Optional.of(department));
        doNothing().when(departmentService).deleteById(department.getId());

        this.mockMvc.perform(delete("/api/departments/{id}", department.getId()))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id", is(department.getId())))
                .andExpect(jsonPath("$.name", is(department.getName())))
                .andExpect(jsonPath("$.leader", is(department.getLeader())));

    }

    @Test
    void shouldReturn404WhenDeletingNonExistingDepartment() throws Exception {
        Integer id = 5;
        given(departmentService.getById(id)).willReturn(Optional.empty());

        this.mockMvc.perform(delete("/api/departments/{id}", id))
                .andExpect(status().isNotFound());

    }


}
