package hu.iqjb.testing.department.system;

import hu.iqjb.testing.department.entity.Department;
import hu.iqjb.testing.department.service.DepartmentService;
import lombok.extern.slf4j.Slf4j;
import org.apache.tomcat.util.codec.binary.Base64;
import org.assertj.core.api.Assert;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ActiveProfiles;

import javax.transaction.Transactional;

import java.nio.charset.Charset;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@Slf4j
@ActiveProfiles("test")
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class SystemTest {

    @Autowired
    private TestRestTemplate testRestTemplate;

    @Autowired
    private DepartmentService departmentService;




    @Test
    @Order(1)
    @Rollback(false)
    public void shouldFindEmployee() {
        Department department = new Department("name2", "leader2");
        department = departmentService.save(department);
        assertThat(department).isNotNull();

//        testRestTemplate.getInterceptors().add(
//                new BasicAuthorizationInterceptor("username", "password"));
        ResponseEntity<Department> response =
                testRestTemplate.getForEntity("/api/departments/1", Department.class);
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(response.getBody()).isNotNull();

    }

    private HttpHeaders createHeaders(String username, String password){
        return new HttpHeaders() {{
            String auth = username + ":" + password;
            byte[] encodedAuth = Base64.encodeBase64(
                    auth.getBytes(Charset.forName("utf8")) );
            String authHeader = "Basic " + new String( encodedAuth );
            set( "Authorization", authHeader );
        }};
    }

    @Test
    @Order(3)
    public void shouldReceiveNotFound() {
        Department department = new Department("name1", "leader1");
        department = departmentService.save(department);
        assertThat(department).isNotNull();

        ResponseEntity<Department> response =
                testRestTemplate.getForEntity("/api/departments/2", Department.class);
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
        assertThat(response.getBody()).isNull();
    }


    @Test
    @Order(3)
    public void shouldInsertDepartment() {
        Department department = new Department("name1", "leader1");

        ResponseEntity<Department> response =
                testRestTemplate.postForEntity("/api/departments",
                        department, Department.class);
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.CREATED);
        assertThat(response.getBody()).isNotNull();
    }

    @Test
    @Order(2)
    @Rollback(true)
    public void shouldNotDelete() {
        Department department = departmentService.getById(1).get();
        assertThat(department).isNotNull();

       testRestTemplate.delete("/api/departments/1", 1);
    }
}
