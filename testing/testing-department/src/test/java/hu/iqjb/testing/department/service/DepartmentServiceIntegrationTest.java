package hu.iqjb.testing.department.service;

import hu.iqjb.testing.department.entity.Department;
import hu.iqjb.testing.department.exception.DepartmentRegistrationException;
import hu.iqjb.testing.department.repository.DepartmentRepository;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.*;

//@RunWith(SpringRunner.class)
@SpringBootTest
class DepartmentServiceIntegrationTest {

    @Autowired
    private DepartmentService departmentService;


    @MockBean
    private DepartmentRepository departmentRepository;

//    @BeforeEach
//    void setUp() {
//
//        Department department1 = new Department("dep1");
//        Department department2 = new Department("dep2");
//        List<Department> departmentList = new ArrayList<>();
//        departmentList.add(department1);
//        departmentList.add(department2);
//        Mockito.when(departmentRepository.findAll()).thenReturn(departmentList);
//    }


    @Test
    void shouldSaveDepartmentSuccessFully() {
        Department department = new Department("name1","leader1");

        given(departmentRepository.findByName(department.getName())).willReturn(Optional.empty());
        given(departmentRepository.save(department)).willAnswer(invocation -> invocation.getArgument(0));

        Department saved = departmentService.save(department);

        assertThat(saved).isNotNull();

        verify(departmentRepository).save(any(Department.class));

    }


    @Test
    void shouldThrowErrorWhenSaveDepartmentWithExistingName() {
        Department department = new Department("name1","leader1");

        given(departmentRepository.findByName(department.getName())).willReturn(Optional.of(department));

        assertThrows(DepartmentRegistrationException.class,() -> {
            departmentService.save(department);
        });

        verify(departmentRepository, never()).save(any(Department.class));
    }


    @Test
    void shouldUpdateUser() {
        Department department = new Department("name1","leader2");

        given(departmentRepository.save(department)).willReturn(department);

        Department expected = departmentService.update(department);

        assertThat(expected).isNotNull();

        verify(departmentRepository).save(any(Department.class));
    }


    @Test
    void shouldReturnFindAll() {
        List<Department> datas = new ArrayList();
        datas.add(new Department("name1","leader1"));
        datas.add(new Department("name2","leader2"));
        datas.add(new Department("name3","leader3"));

        given(departmentRepository.findAll()).willReturn(datas);

        List<Department> expected = departmentService.getAll();

        assertEquals(expected, datas);
    }


    @Test
    void shouldFindDepartmentById(){
        Integer id = 1;
        Department department = new Department("name1","leader1");

        given(departmentRepository.findById(id)).willReturn(Optional.of(department));

        Optional<Department> expected  =departmentService.getById(id);

        assertThat(expected.get()).isNotNull();

    }

    @Test
    void shouldDelete() {
        Integer id =1;

        departmentService.deleteById(id);
        departmentService.deleteById(id);

        verify(departmentRepository, times(2))
                .deleteById(id);
    }

}