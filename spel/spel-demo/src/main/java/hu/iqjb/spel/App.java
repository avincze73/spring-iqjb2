package hu.iqjb.spel;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class App 
{
    public static void main( String[] args )
    {
        AnnotationConfigApplicationContext applicationContext = new AnnotationConfigApplicationContext(MyConfig.class);
        Employee employee = applicationContext.getBean("employee", Employee.class);
        System.out.println(employee.getCountry());
        System.out.println(employee.getFullAddress());

        Calculator calculator = applicationContext.getBean("calculator", Calculator.class);
        System.out.println(calculator.isEqual());

        CustomerService customerService = applicationContext.getBean("customerService", CustomerService.class);
        System.out.println(customerService.getListElement());
        System.out.println(customerService.getMapElement());

        EmailClient emailClient = applicationContext.getBean("emailClient", EmailClient.class);
        System.out.println(emailClient.getValidEmail());

    }
}
