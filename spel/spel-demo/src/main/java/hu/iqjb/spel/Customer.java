package hu.iqjb.spel;

import lombok.Getter;
import lombok.Setter;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
@Getter
@Setter
public class Customer {

    private Map<String, String> map;
    private List<String> list;
    public Customer() {
        map = new HashMap<>();
        map.put("emp1", "Employee 1");
        map.put("emp2", "Employee 2");
        list = new ArrayList<>();
        list.add("employee-1");
        list.add("employee-2");
    }


}
