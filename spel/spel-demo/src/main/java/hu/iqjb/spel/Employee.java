package hu.iqjb.spel;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Employee {

    @Value("Kiss Péter")
    private String custName;
    @Value("#{address}")
    private Address address;
    @Value("#{address.country}")
    private String country;
    @Value("#{address.getFullAddress()}")
    private String fullAddress;


}
