package hu.iqjb.spel;

import lombok.Getter;
import lombok.Setter;
import org.springframework.stereotype.Component;

@Component
@Getter
@Setter
public class Number {

    private Integer no;

}
