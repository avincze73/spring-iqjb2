package hu.iqjb.jdbc.main;

import hu.iqjb.jdbc.dao.EmployeeDao;
import hu.iqjb.jdbc.model.Employee;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class HrPayrollSystem {

    public static void main(String[] args) {

        @SuppressWarnings("resource")
        ApplicationContext context = new ClassPathXmlApplicationContext(
                "beans.xml");
        EmployeeDao employeeDao = context.getBean("employeeDaoImp",
                EmployeeDao.class);

        // create employee table
        employeeDao.createEmployee();

        System.out.println(employeeDao.getEmployeeCount());

        Employee emp = new Employee(1, "Attila", 40);
        employeeDao.insertEmployee(emp);

        Employee employee = employeeDao.getEmployeeById(1);
        System.out.println(employee.getEmpId() + " - " + employee.getName());

        //Employee employee2 = employeeDao.getEmployee2(1);
        //System.out.println(employee2.getEmpId() + " - " + employee2.getName());

        System.out.println(employeeDao.deleteEmployeeById(1));

        System.out.println(employeeDao.getEmployeeCount());

    }

}
