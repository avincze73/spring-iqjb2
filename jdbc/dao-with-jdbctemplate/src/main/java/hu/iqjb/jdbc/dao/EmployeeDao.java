package hu.iqjb.jdbc.dao;

import hu.iqjb.jdbc.model.Employee;

public interface EmployeeDao {

    void createEmployee();

    int getEmployeeCount();

    int insertEmployee(Employee employee);

    int deleteEmployeeById(int empId);

    Employee getEmployeeById(int empId);

    Employee getEmployee2(Integer id);

}
