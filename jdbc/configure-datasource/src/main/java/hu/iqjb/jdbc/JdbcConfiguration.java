package hu.iqjb.jdbc;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

import javax.sql.DataSource;

@Configuration
public class JdbcConfiguration {
    @Bean
    //It returns a new connection every time a connection is requested by the getConnection() method.
    //This datasource does not have pooling capability
    //C3PO and Apache Commons DBCP have this feature.
    public DataSource dataSource() {
        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName("com.mysql.jdbc.Driver");
        dataSource.setUrl("jdbc:mysql://localhost:3306/spring");
        dataSource.setUsername("spring");
        dataSource.setPassword("titkos123");
        return dataSource;
    }
}
