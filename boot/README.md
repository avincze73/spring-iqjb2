# Spring Boot

![Spring boot](./images/boot1.jpg)

## First spring boot application
REST demo

## Spring boot with actuator

### actuator-service project

@Controller vs @RestController
- The key difference between a human-facing controller and a REST endpoint controller is in how the response is created. 
- Rather than rely on a view (such as thymeleaf) to render model data in HTML, an endpoint controller returns the data to be written directly to the body of the response.
- Because Jackson 2 is in the classpath, MappingJackson2HttpMessageConverter will handle the conversion of a Greeting object to JSON if the request’s Accept header specifies that JSON should be returned.

``` sh
curl localhost:9000/
curl localhost:9000/hello-world
curl localhost:9001/actuator/health
```


### Spring boot actuator via JMX console

- jconsole


### spring-boot-actuator project

- Custom endpoints
- Built-in endpoints
- HTTP vs JMX access

``` bash
curl http://localhost:8080/actuator/health
curl http://localhost:8080/actuator/info

curl http://localhost:8080/actuator/configprops
curl http://localhost:8080/actuator/httptrace
curl http://localhost:8080/actuator/mappings
curl http://localhost:8080/actuator/scheduledtasks
curl http://localhost:8080/actuator/metrics
curl http://localhost:8080/actuator/shutdown
curl http://localhost:8080/actuator/configprops


curl -X POST \
  http://localhost:8080/actuator/shutdown\
  -H 'Content-Type: application/json' \
  -H 'Postman-Token: 4b337573-ed7c-48f4-b281-c8c53f47dde8' \
  -H 'cache-control: no-cache' 
  


curl -X POST \
  http://localhost:8080/actuator/custom-rest-endpoint \
  -H 'Content-Type: application/json' \
  -H 'Postman-Token: 4b337573-ed7c-48f4-b281-c8c53f47dde8' \
  -H 'cache-control: no-cache' \
  -d '{
"request":"This is my request"
}'


curl -X GET \
  http://localhost:8080/actuator/custom-endpoint \
  -H 'Postman-Token: d26ab39f-36bf-4165-b00c-855b2dbfca9d' \
  -H 'cache-control: no-cache'




```



