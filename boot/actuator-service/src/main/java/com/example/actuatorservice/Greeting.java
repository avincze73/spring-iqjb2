package com.example.actuatorservice;

import ch.qos.logback.core.joran.spi.NoAutoStart;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;

import javax.xml.bind.annotation.XmlRootElement;


@JacksonXmlRootElement
public class Greeting {

    private  long id;
    private  String content;

    public Greeting() {
        this.id = 1;
        this.content = "hello";
    }

    public Greeting(long id, String content) {
        this.id = id;
        this.content = content;
    }

    public long getId() {
        return id;
    }

    public String getContent() {
        return content;
    }

}
