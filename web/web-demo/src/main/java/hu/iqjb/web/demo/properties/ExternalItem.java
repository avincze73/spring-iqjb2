package hu.iqjb.web.demo.properties;


import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ExternalItem {

    private String name;
    private int size;
}
