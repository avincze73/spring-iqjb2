package hu.iqjb.web.demo.properties;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

@Configuration
public class ExternalProperties {

    @Autowired
    private RestTemplate restTemplate;


    @Bean
    @ConfigurationProperties(prefix = "externalitem")
    public ExternalItem externalItem() {
        return new ExternalItem();
    }

    @Bean
    public RestTemplate restTemplate(RestTemplateBuilder builder) {
        return builder.build();
    }
}
