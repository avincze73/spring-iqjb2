package hu.iqjb.web.demo.properties;

import hu.iqjb.web.demo.servlet.IqjbServlet;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class IqjbServletConfig {


    @Bean
    public ServletRegistrationBean exampleServletBean() {
        ServletRegistrationBean bean = new ServletRegistrationBean(
                new IqjbServlet(), "/iqjbservlet/*");
        bean.setLoadOnStartup(1);
        return bean;
    }
}
