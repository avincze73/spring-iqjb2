package hu.iqjb.web.demo.properties;

import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.PropertySources;

@Configuration
@PropertySources({
        @PropertySource("classpath:foo.properties"),
        @PropertySource("classpath:bar.properties")
})
@Getter
@Setter
public class PropertiesWithJavaConfig {

    @Value("${foo.name}")
    private String fooName;

    @Value("${bar.name}")
    private String barName;

    @Value("${bar.default:default string}")
    private String bardefault;
}
