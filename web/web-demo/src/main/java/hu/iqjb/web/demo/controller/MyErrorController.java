package hu.iqjb.web.demo.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServletRequest;

//@Controller
@Slf4j
public class MyErrorController /*implements ErrorController*/ {
    //@Override
    public String getErrorPath() {
        return "/error";
    }

//    @RequestMapping("/error")
//    public String handleError(HttpServletRequest request) {
//        log.info("Error occured");
//        Object status = request.getAttribute(RequestDispatcher.ERROR_STATUS_CODE);
//
//        if (status != null) {
//            Integer statusCode = Integer.valueOf(status.toString());
//
//            if(statusCode == HttpStatus.ACCEPTED.value()) {
//                return "404";
//            }
//            else if(statusCode == HttpStatus.INTERNAL_SERVER_ERROR.value()) {
//                return "500";
//            }
//        }
//        return "/error";
//    }
}
