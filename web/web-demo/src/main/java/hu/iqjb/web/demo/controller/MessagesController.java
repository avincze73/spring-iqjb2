package hu.iqjb.web.demo.controller;

import hu.iqjb.web.demo.properties.IqjbProperties;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@Slf4j
public class MessagesController {

    @Autowired
    private MessageInterpreter messageInterpreter;

    @RequestMapping("/messages")
    public String main(Model model) {
        model.addAttribute("greeting", messageInterpreter.getMessage("greeting"));
        log.info(messageInterpreter.getMessage("greeting"));
        return "/messages";
    }
}
