package hu.iqjb.jpa;

public interface BookDao {
    public void save(Book book);
}
