package hu.iqjb.jpa;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class Main {

    public static void main(String[] args) {
        ApplicationContext applicationContext
                = new AnnotationConfigApplicationContext(MyConfiguration.class);

        BookService bookService
                = applicationContext
                .getBean(BookService.class);

        Book book = new Book();
        book.setName("book1");
        //bookService.save(book);


        ServiceFacade serviceFacade = applicationContext
                .getBean(ServiceFacade.class);
        book = new Book();
        book.setName("book2");
        Department department = new Department();
        department.setName("department1");

        try {
            serviceFacade.save(book, department);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

}
