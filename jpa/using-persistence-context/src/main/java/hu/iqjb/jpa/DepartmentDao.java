package hu.iqjb.jpa;

public interface DepartmentDao {
    public void save(Department department);
}
