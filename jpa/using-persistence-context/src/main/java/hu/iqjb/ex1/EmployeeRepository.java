package hu.iqjb.ex1;

import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;
import java.util.Set;

@Repository
public class EmployeeRepository {

	@PersistenceContext
	private EntityManager entityManager;


	void add(Employee employee){
		entityManager.persist(employee);

	}

	List<Employee> getAll(){
		return entityManager.createQuery("select e from Employee e", Employee.class).getResultList();
	}
}
