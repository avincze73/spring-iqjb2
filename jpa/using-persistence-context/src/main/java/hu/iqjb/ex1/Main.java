package hu.iqjb.ex1;

import hu.iqjb.jpa.Book;
import hu.iqjb.jpa.BookService;
import hu.iqjb.jpa.MyConfiguration;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class Main {

    public static void main(String[] args) {
        ApplicationContext applicationContext
                = new AnnotationConfigApplicationContext(MyConfig.class);

        EmployeeService employeeService
                = applicationContext
                .getBean(EmployeeService.class);


        Employee employee = new Employee();
        employee.setId(1);
        employee.setFirstName("firstName1");
        employeeService.add(employee);
    }

}
