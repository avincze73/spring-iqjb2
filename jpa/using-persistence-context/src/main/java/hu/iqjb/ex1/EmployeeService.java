package hu.iqjb.ex1;

import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Set;

@Service
@Scope(scopeName="prototype")
@Setter
@Transactional
public class EmployeeService {
	
	@Autowired
	private EmployeeRepository employeeRepository;

	public void add(Employee employee) {
		employeeRepository.add(employee);
	}

	public List<Employee> getAll() {
		return employeeRepository.getAll();
	}

}
