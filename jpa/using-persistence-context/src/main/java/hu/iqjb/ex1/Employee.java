package hu.iqjb.ex1;


import lombok.*;

import javax.persistence.Entity;
import javax.persistence.Id;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
@ToString
@Entity(name = "Employee")
public class Employee {
	@Id
	private int id;
	private String firstName;
	private String lastName;
	private String loginName;
	private String password;
	private String title;
	private double salary;

}
